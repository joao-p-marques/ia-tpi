
###

# João Marques
# NMEC: 89234
# LEI

###

import math

from tree_search import *

class MyNode(SearchNode):
    def __init__(self, state, parent=None, depth=0, cost=0, heuristic=0, evalfunc=0): 
        self.state = state
        self.parent = parent
        self.depth = depth
        self.cost = cost
        self.heuristic = heuristic
        self.evalfunc = evalfunc
        self.my_evalfunc = True
        self.children = []
    def reset(self): # set control flag to recieve values from children
        self.my_evalfunc = True
        if self.parent is None:
            return
        self.parent.reset()
    def add_children(self, node):
        self.children.append(node)
    def remove_children(self, node):
        self.children.remove(node)
    def propagate_evalfunc(self):
        m = None
        for child in self.children:
            if (m is None) or (child.evalfunc < m):
                m = child.evalfunc
        self.evalfunc = m
    def eval_func(self):
        self.evalfunc = self.cost + self.heuristic
    def __str__(self):
        return "no(" + str(self.state) + "," + str(self.parent) + ")"
    def __repr__(self):
        return str(self)

class MyTree(SearchTree):

    def __init__(self, problem, strategy='breadth', max_nodes=None): 
        self.problem = problem
        self.strategy = strategy
        self.max_nodes = max_nodes
        self.root = MyNode(
                problem.initial, 
                None, 
                0, 
                0,
                self.problem.domain.heuristic(
                    self.problem.initial, 
                    self.problem.goal
                    ),
                0
                )
        self.open_nodes = [self.root]
        self.solution_cost = 0
        self.solution_length = 0
        self.total_nodes = 1
        self.nodes_in_memory = 1
        self.terminal_nodes = 1 # root node is terminal
        self.non_terminal_nodes = 0
        self.no_child_nodes = [] # list of nodes where children are leaf

    def astar_add_to_open(self,lnewnodes):
        self.open_nodes = sorted(
                self.open_nodes + lnewnodes,
                key = lambda node: node.evalfunc
                )

    def effective_branching_factor(self):
        N = self.total_nodes
        d = float(self.solution_length)
        
        incr = 0.000005

        if self.solution_length > 3:
            error = 0.001
        else:
            error = 0.0001

        while True:
            b = pow(N, 1/d)
            s = 0
            for i in range(self.solution_length+1):
                s += pow(b, i)

            if abs(s - N) <= error:
                break
            d += incr
        return b

    def update_ancestors(self,node):
        if node.parent is None:
            return

        if node.parent.my_evalfunc or node.parent.evalfunc > node.evalfunc:
            node.parent.evalfunc = node.evalfunc
            node.parent.my_evalfunc = False

        return self.update_ancestors(node.parent)

    def get_no_children_nodes(self, node):
        if not node.children:
            return

        to_add = True
        for child in node.children:
            if not child.children and not node.state in [n.state for n in self.no_child_nodes]:
                self.no_child_nodes.append(node)
            else:
                self.get_no_children_nodes(child)

    def discard_worse(self):
        self.get_no_children_nodes(self.root)
        nodes_to_eval = sorted(self.no_child_nodes, key=lambda node:node.evalfunc) 
        worst_nodes = nodes_to_eval[::-1]
        worst_node = worst_nodes[0]

        # remove
        self.nodes_in_memory -= len(worst_node.children)
        self.terminal_nodes -= len(worst_node.children)
        self.non_terminal_nodes -= 1
        self.terminal_nodes += 1
        for child in worst_node.children:
            if not child in self.open_nodes:
                pass
            else:
                self.non_terminal_nodes -= 1
                self.open_nodes.remove(child)
        self.open_nodes.append(worst_node)

        self.no_child_nodes = []
        # self.max_nodes = None

    def calc_length(self, path):
        for node in path:
            if node != self.root:
                self.solution_length += 1

    def get_path(self, node):
        if node.parent == None:
            return [node.state], [node]
        path, path_full = self.get_path(node.parent)
        path += [node.state]
        path_full += [node]
        return path, path_full

    def search2(self):
        while self.open_nodes != []:
            if self.max_nodes is not None and self.nodes_in_memory > self.max_nodes:
                self.discard_worse()
            node = self.open_nodes.pop(0)
            if self.problem.goal_test(node.state):
                self.solution_cost = node.cost # calculate solution cost
                path, path_full = self.get_path(node) # path_full contains full node
                self.calc_length(path_full) # calculate length
                return path
            lnewnodes = []
            for a in self.problem.domain.actions(node.state):
                newstate = self.problem.domain.result(node.state,a)
                path, full_path = self.get_path(node)
                if not newstate in path: # check for loops
                    newnode = MyNode(
                            newstate,
                            node,
                            node.depth + 1,
                            node.cost + self.problem.domain.cost(node.state, a),
                            self.problem.domain.heuristic(newstate, self.problem.goal)
                            )
                    newnode.eval_func() # calc evalfunc
                    node.add_children(newnode)
                    lnewnodes.append(newnode)
                    self.total_nodes += 1
                    self.nodes_in_memory += 1
            if lnewnodes:
                self.terminal_nodes -= 1
                self.terminal_nodes += len(lnewnodes)
                self.non_terminal_nodes += 1
                node.reset() # method for update_ancestors()
                for n in lnewnodes:
                    self.update_ancestors(n)
            self.add_to_open(lnewnodes)
        return None

    def add_to_open(self, lnewnodes):
        if self.strategy == 'breadth':
            self.open_nodes.extend(lnewnodes)
        elif self.strategy == 'depth':
            self.open_nodes[:0] = lnewnodes
        elif self.strategy == 'astar':
            self.astar_add_to_open(lnewnodes)
        elif self.strategy == 'uniform':
            pass

    # shows the search tree in the form of a listing
    def show(self,heuristic=False,node=None,indent=''):
        if node==None:
            self.show(heuristic,self.root)
            print('\n')
        else:
            line = indent+node.state
            if heuristic:
                line += (' [' + str(node.evalfunc) + ']')
            print(line)
            if node.children==None:
                return
            for n in node.children:
                self.show(heuristic,n,indent+'  ')


